Module SMS Gateway
==================


1. Form to ask a free test
**************************

In the General settings, You can send a request for free test via a form. You can test the module with a free account.

 .. image:: http://image.noelshack.com/fichiers/2016/38/1474441629-form-vue-formulaire-2.png




2. Configuration
****************
In the General settings, you have the configuration to add your personal account information.
When you add this configuration, It offers 25 free SMS to test module's functionality.


 .. image:: http://image.noelshack.com/fichiers/2016/38/1474441550-config-demo-2.png
 
 			