import simplejson
import urllib
import openerp
from openerp import http
from openerp.http import request
import openerp.addons.web.controllers.main as webmain
from openerp.addons.web.http import SessionExpiredException
from werkzeug.exceptions import BadRequest
import werkzeug.utils
import time
import datetime
from openerp import models, fields, api, _

class mtarget(http.Controller):
    
    @http.route('/mtarget/reponse', methods=['POST'], type='http', auth='public',csrf=False)
    def dash_phase(self, **kw):
        #request.session.authenticate(request.db, "admin", "admin")
        connect_obj = request.env['message.message'].sudo()
        #kw.get('test')
        
        html="""
        <!DOCTYPE html>
        <html>
        <head>
        <meta charset=\"utf-8\" />
        <title>Success</title>
        </head>
        <body>
        OK</body></html>"""    
        
        #statut_tab =  kw.get("Status")
        message_id_tab =  kw.get("MsgId")
        statut =  kw.get("StatusText")
        connect_id = connect_obj.search([('mess_id','=',kw.get("MsgId"))])
        if connect_id:
            #connect_id.deliverystatus = statut
            val={
                'deliverystatus': statut,
            }
            connect_id.write(val)
        
        return html
