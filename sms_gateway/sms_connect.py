# -*- coding: utf-8 -*-
######################################################################################################
#
# Copyright (C) B.H.C. sprl - All Rights Reserved, http://www.bhc.be
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied,
# including but not limited to the implied warranties
# of merchantability and/or fitness for a particular purpose
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
######################################################################################################

import urllib
import httplib
from openerp.exceptions import RedirectWarning, UserError, ValidationError
import json
import base64
from openerp.addons.foundation.JSONRequest import JSONRequest

import re
import logging
from openerp import models, fields, api, _
_logger = logging.getLogger(__name__)
_TASK_STATE = [('sent', 'Sent'),('notsent', 'Error')]

class menu_connections_reception(models.Model): 
    _name='connect.reception'
    _order='sequence asc'
    
    connect_id = fields.Many2one('connect.connect',ondelete="cascade",string='Connections')
    name = fields.Char(string='Second keyword',help="Used in OpenERP. If you want all messages create records for only one model, set value '*'")
    link = fields.Many2one('ir.model',string='Applies to' ,required="True")
    subject = fields.Many2one('ir.model.fields',string='Subject', domain="[('model_id','=',link),('ttype','!=','one2many'),('ttype','!=','many2many')]")
    message = fields.Many2one('ir.model.fields',string='Message', domain="[('model_id','=',link),('ttype','!=','one2many'),('ttype','!=','many2many')]")
    mobile = fields.Many2one('ir.model.fields',string='Mobile', domain="[('model_id','=',link),('ttype','!=','one2many'),('ttype','!=','many2many')]")
    sms_id = fields.Many2one('ir.model.fields',string='Message ID', domain="[('model_id','=',link),('ttype','!=','one2many'),('ttype','!=','many2many')]")
    date = fields.Many2one('ir.model.fields',string='Date', domain="[('model_id','=',link),'|',('ttype','=','date'),('ttype','=','datetime')]")
    sequence = fields.Integer(string='Sequence')

    @api.onchange('name')
    def onchange_name(self):
        if self.name=='*':
            return {
                'warning': {
                    'title': "Warning!",
                    'message': "With '*', only this configuration will be used",
                },
            }
        
    @api.onchange('link')
    def onchange_link(self):
        self.subject=False
        self.message=False
        self.mobile=False
        self.sms_id=False
        self.date=False
        
menu_connections_reception()

class menu_connections(models.Model): 
    _name='connect.connect'
    
    type = fields.Selection([('sending', 'Sending')], string='Type',required=True) #type = fields.Selection([('sending', 'Sending'),('reception', 'Reception'),('payment', 'Payment'),('location', 'Location')], string='Type',required=True)
    username = fields.Char(string='Username' , required=True, )
    password = fields.Char(string='Password' ,  required=True)
    name = fields.Char(string='Name',required=True)
    url = fields.Char(string='Url',required=True)
    registration_id = fields.Char(string='ServiceId',required=True)
    keyword = fields.Char(string='First keyword',help="Used for Belgacom")
    type_2 = fields.Selection([('shared', 'Shared'),('dedicated', 'Dedicated')],string='Type',help="If dedicated,OpenERP used the first word in the message; if shared, the second to create record")
    connect_ids = fields.One2many('connect.reception', 'connect_id', string='Connections')
     
    @api.constrains('url')
    def _check_url(self):
        for i in self:
            if i.url:
                url=i.url
                if url[0:7] != "http://" : 
                    raise ValidationError(_("An url starts like this : http:// "))
     
    """
    @api.model
    def create(self, vals):
        config = self.search([('type','=','sending')])
        if config:
            raise Warning(_('You can have only one sending configuration!'))
        res = super(menu_connections, self).create(vals)
        return res
    """
menu_connections()

class message_send(models.Model):
    _name='message.message'
    _order='id desc'
    
    number= fields.Char(string='Number',readonly=True)
    deliverystatus = fields.Char(string='Delivery Status',readonly=True)
    message = fields.Text(string='Message',readonly=True)
    datime = fields.Datetime(string='Date',readonly=True)
    template = fields.Many2one('email.template','Template',readonly=True)
    partner_id = fields.Many2one('res.partner','Partner', readonly=True)
    state = fields.Selection(_TASK_STATE, 'Related Status', required=True)
    mess_id = fields.Char(string='Message Id',readonly=True)

