# -*- coding: utf-8 -*-
######################################################################################################
#
# Copyright (C) B.H.C. sprl - All Rights Reserved, http://www.bhc.be
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied,
# including but not limited to the implied warranties
# of merchantability and/or fitness for a particular purpose
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
######################################################################################################

{
    "name" : "SMS Gateway",
    "version" : "1.1",
    "author" : "BHC",
    "website" : "www.bhc.be",
    "category" : "Generic Modules/Others",
    "depends" : ["base","sale","mail","crm"],
    "description" : """
    This module provides an integration with the french telecom operator M-Target.\n
    Currently it allows to send SMS to:\n
    * Send SMS: From a predefined template (like the email ones) \n
    * Send SMS: via a text \n
    * Send multiple SMS: choose your partners you want to send SMS \n
    
    In order to configure your account, you first need to submit a form for a free test account

    """,
    'images': ['images/main_screenshot.png',],
    "init_xml" : [],
    "demo_xml" : [],
    "data":[
            #"reception_sms_view.xml",
            'wizard/sms_template_preview_view.xml',
            'wizard/sms_compose_message_view.xml',
            'sms_connect_view.xml',
            'wizard/subscription_view.xml',
            'parametre_view.xml',
            'security/ir.model.access.csv',
            ],
    "demo": [
        'sms_demo.xml',
        ],     
    "active": False,
    "installable": True,
}
