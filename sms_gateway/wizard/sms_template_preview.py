# -*- coding: utf-8 -*-
######################################################################################################
#
# Copyright (C) B.H.C. sprl - All Rights Reserved, http://www.bhc.be
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied,
# including but not limited to the implied warranties
# of merchantability and/or fitness for a particular purpose
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
######################################################################################################

import base64
import logging
from openerp import netsvc
from openerp import tools
from openerp import models, fields, api, _
from urllib import urlencode, quote as quote
_logger = logging.getLogger(__name__)

class mail_template(models.Model):
    _inherit = "mail.template"
    
    number = fields.Char(string='Number')
    content= fields.Text(string='Content')
    sms = fields.Boolean(string='Template SMS',default=0)
    mail = fields.Boolean(string='Template Mail',default=1)
    
    @api.one
    def generate_SMS(self, template_id, res_id):
        template = self.get_mail_template(template_id, res_id)
        values = {}
        for field in ['number', 'content']:
            values[field] = self.render_template(getattr(template, field),
                                                 template.model, res_id) \
                                                 or False
        return values
mail_template()

class sms_template_preview(models.TransientModel):
    _inherit = "mail.template"
    _name = "sms_template.preview"
    _description = "SMS Template Preview"

    @api.model
    def _get_records(self):
        context = self._context
        template_id = context.get('template_id', False)
        if not template_id:
            return []
        mail_template = self.env['mail.template']
        template = mail_template.browse(template_id)
        model =  self.env[template.model_id.model]
        record_ids = model.search([], 0, 10, 'id')
        default_id = context.get('default_res_id')
        if default_id and default_id not in record_ids:
            record_ids.insert(0, default_id)
        return record_ids.name_get()
    
    res_id = fields.Selection('_get_records', string='Sample Document')
    
    @api.cr_uid_context
    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        result = super(sms_template_preview, self).default_get(cr, uid, fields, context=context)
        mail_template = self.pool.get('mail.template')
        template_id = context.get('template_id')
        if 'res_id' in fields and not result.get('res_id'):
            records = self._get_records(cr, uid, context=context)
            result['res_id'] = records and records[0][0] or False # select first record as a Default
        if template_id and 'model_id' in fields and not result.get('model_id'):
            result['model_id'] = mail_template.read(cr, uid, int(template_id), ['model_id'], context).get('model_id', False)
        return result

    @api.onchange('res_id')
    def on_change_res_id(self):
        if not self.res_id: return {}
        vals = {}
        mail_template = self.env['mail.template']
        template_id = self._context and self._context.get('template_id')
        template = mail_template.browse(template_id)
        vals['name'] = template.name
        mail_values = template.generate_SMS(template_id, self.res_id)
        for k in ('number','content'):
            vals[k] = mail_values[0][k]
        self.number=vals['number']
        self.content = vals['content']
