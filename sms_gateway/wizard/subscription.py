# -*- coding: utf-8 -*-
######################################################################################################
#
# Copyright (C) B.H.C. sprl - All Rights Reserved, http://www.bhc.be
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied,
# including but not limited to the implied warranties
# of merchantability and/or fitness for a particular purpose
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
######################################################################################################

from datetime import datetime
import time
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import models, fields, api, _
from dateutil.relativedelta import *
import unicodedata
from openerp.exceptions import RedirectWarning, UserError, ValidationError

class subscription(models.Model):
    _name = "subscription.mtarget"
    _description = "subscription request for test module" 
     
    company_name = fields.Char(string='Company Name',required=True)
    siret = fields.Char(string='Siret',required=True)
    address = fields.Char(string='Address',required=True)
    zip = fields.Char(string='Postal Code', size=24)
    city = fields.Char(string='City')
    country = fields.Many2one('res.country', 'Country', ondelete='restrict',required=True)
    mail = fields.Char(string='Email', required=True)
    ip = fields.Char(string='Call back url', help='IP address to receive the Delivery Status of SMS')
    
    @api.multi
    def send(self):
        mail_table = self.env['mail.mail']
        user_table = self.env['res.users']
        mail_ids=[]
        self.company_name = unicodedata.normalize('NFKD', self.company_name).encode('ASCII','ignore').decode('ASCII')
        self.siret= unicodedata.normalize('NFKD', self.siret).encode('ASCII','ignore').decode('ASCII')
        self.address = unicodedata.normalize('NFKD', self.address).encode('ASCII','ignore').decode('ASCII')
        #self.country = unicodedata.normalize('NFKD', self.country).encode('ASCII','ignore').decode('ASCII')
        if self.zip : 
            self.zip = unicodedata.normalize('NFKD', self.zip).encode('ASCII','ignore').decode('ASCII')
            if self.city :
                self.city = unicodedata.normalize('NFKD', self.city).encode('ASCII','ignore').decode('ASCII')
                if self.mail : 
                    self.mail = unicodedata.normalize('NFKD', self.mail).encode('ASCII','ignore').decode('ASCII')
                    if self.ip :
                        self.ip = unicodedata.normalize('NFKD', self.ip).encode('ASCII','ignore').decode('ASCII') 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Code postal : '+str(self.zip.decode('latin-1'))+'<br/> Ville : '+str(self.city.decode('latin-1'))+'<br/> Pays : '+str(self.country.name.decode('latin-1'))+'<br/> Email : '+str(self.mail.decode('latin-1'))+ '<br/> Adresse IP : '+str(self.ip.decode('latin-1')) 
                    else : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Code postal : '+str(self.zip.decode('latin-1'))+'<br/> Ville : '+str(self.city.decode('latin-1'))+'<br/> Pays : '+str(self.country.name.decode('latin-1'))+'<br/> Email : '+str(self.mail.decode('latin-1')) 
                else :
                    if self.ip : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Code postal : '+str(self.zip.decode('latin-1'))+'<br/> Ville : '+str(self.city.decode('latin-1'))+ '<br/> Adresse IP : '+str(self.ip.decode('latin-1')) 
                    else : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Code postal : '+str(self.zip.decode('latin-1'))+'<br/> Ville : '+str(self.city.decode('latin-1'))
            else :                  
                if self.mail : 
                    if self.ip : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Code postal : '+str(self.zip.decode('latin-1'))+'<br/> Pays : '+str(self.country.name.decode('latin-1'))+'<br/> Email : '+str(self.mail.decode('latin-1'))+ '<br/> Adresse IP : '+str(self.ip.decode('latin-1')) 
                    else : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Code postal : '+str(self.zip.decode('latin-1'))+'<br/> Pays : '+str(self.country.name.decode('latin-1'))+'<br/> Email : '+str(self.mail.decode('latin-1')) 
                else :
                    if self.ip : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Code postal : '+str(self.zip.decode('latin-1'))+ '<br/> IP Address : '+str(self.ip.decode('latin-1')) 
                    else : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Code postal : '+str(self.zip.decode('latin-1'))
        else : 
            if self.city :
                if self.mail : 
                    if self.ip : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Ville : '+str(self.city.decode('latin-1'))+'<br/> Pays : '+str(self.country.name.decode('latin-1'))+'<br/> Email : '+str(self.mail.decode('latin-1'))+ '<br/> Adresse IP : '+str(self.ip.decode('latin-1')) 
                    else : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Ville : '+str(self.city.decode('latin-1'))+'<br/> Pays : '+str(self.country.name.decode('latin-1'))+'<br/> Email : '+str(self.mail.decode('latin-1')) 
                else :
                    if self.ip : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Ville : '+str(self.city.decode('latin-1'))+ '<br/> Adresse IP : '+str(self.ip.decode('latin-1')) 
                    else : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Ville : '+str(self.city.decode('latin-1'))
            else :                  
                if self.mail : 
                    if self.ip : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Pays : '+str(self.country.name.decode('latin-1'))+'<br/> Email : '+str(self.mail.decode('latin-1'))+ '<br/> Adresse IP : '+str(self.ip.decode('latin-1')) 
                    else : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Pays : '+str(self.country.name.decode('latin-1'))+'<br/> Email : '+str(self.mail.decode('latin-1')) 
                else :
                    if self.ip : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))+'<br/> Adresse IP : '+str(self.ip.decode('latin-1')) 
                    else : 
                        body = 'Societe : '+str(self.company_name.decode('latin-1'))+'<br/> Siret : '+str(self.siret.decode('latin-1'))+'<br/> Adresse : '+str(self.address.decode('latin-1'))
        mail_values = {
                    'subject': 'Demande pour test gratuit - du module Odoo',
                    'email_to': 'marketing@mtarget.fr',                                                
                    'body_html': body,
                    'body': 'body',
                }
        mail_mail_obj = mail_table.create(mail_values)
        mail_mail = mail_ids.append(mail_mail_obj.id)
        
        
        
        mail_values_confirmation = {
                    'subject': 'Confirmation demande pour test gratuit - du module Odoo',
                    'email_to': self.mail,                                                    
                    'body_html': "Hello,<br/><br/>Your request for a free test account has been received.<br>/You'll receive within one working day another email with all needed details to configure your Odoo SMS Gateway free account.<br/><br/>Regards."
,
                    'body': 'body',
                }
        mail_mail_obj_confirmation = mail_table.create(mail_values_confirmation)
        mail_mail_confirmation = mail_ids.append(mail_mail_obj_confirmation.id)
        
        return True 
    
    @api.constrains('ip')
    def _check_ip(self):
        for i in self:
            if i.ip:
                ip=i.ip
                if ip[0:7] != "http://" : 
                    raise ValidationError(_("An adress ip starts like this : http:// "))

