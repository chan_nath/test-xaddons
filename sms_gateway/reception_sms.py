# -*- coding: utf-8 -*-
######################################################################################################
#
# Copyright (C) B.H.C. sprl - All Rights Reserved, http://www.bhc.be
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied,
# including but not limited to the implied warranties
# of merchantability and/or fitness for a particular purpose
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
######################################################################################################

import urllib
import urllib2
import httplib

from urllib2 import HTTPError
from datetime import datetime
import json
import base64
from  openerp.addons.response.HTTPResponse import HTTPResponse
from urllib2 import HTTPError
#from openerp.addons.response.sms.SendSMSResponse import SendSMSResponse
#from openerp.addons.response.sms.RetrieveSMSResponse import RetrieveSMSResponse 
from openerp.addons.foundation.JSONRequest import JSONRequest
#from openerp.addons.response.sms.SMSSendDeliveryStatusResponse import SMSSendDeliveryStatusResponse
import re
import logging
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from openerp import models, fields, api, _
_logger = logging.getLogger(__name__)

class menu_receipt(models.Model):
    _name = 'receipt.receipt'
    config = fields.Many2one('connect.connect','Configuration',readonly=True)
    link = fields.Many2one('connect.reception','Linked To',readonly=True)
    sendadres = fields.Char(string='Sender', readonly=True)
    mess = fields.Text('Message',readonly=True)
    datime = fields.Datetime('Date',readonly=True)
    mess_id = fields.Char(string='Message Id',readonly=True)
    
    _order='id desc' 
    
    @api.v7
    def cron_receiveSMS(self, cr, uid, context=None):
        connect_obj = self.pool.get('connect.connect')# rÃ©cup field many2one
        connect_recept_obj = self.pool.get('connect.reception')
        receipt_ids = connect_obj.search(cr,uid,[('type','=',"reception")])#recherche obj 
        for j in connect_obj.browse(cr, uid, receipt_ids):# pour chaque obj rÃ©cup sms on exÃ©cute 
            
            url=str(j.url)
            
            
            password=str(j.password) 
            username =str(j.username)
          
            registration_id =str(j.registration_id)
         
            if '{registration_id}' in url: url=url.replace('{registration_id}',str(registration_id))#
       
            responsetype='application/json'#debut instructions propres a lecture  Json
            opener = urllib2.build_opener(urllib2.HTTPHandler)
            request = urllib2.Request(url)
           
            request.add_header('Accept', responsetype)
            request.get_method = lambda: 'GET'
            base64string=base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
            
            request.add_header("Authorization", "Basic %s" % base64string)
            response=HTTPResponse(None)
             
            #try:
            if 1==1:#passe d'office test?
                handle = opener.open(request)
              
                jsondata=json.loads(handle.read())
                
                for i in jsondata['inboundSMSMessageList']['inboundSMSMessage']:
                    mobile=i['senderAddress'].split(':')
                    date=re.sub("T", " ", i['dateTime'])# crÃ©ation datetime param inboundSmsMessage
                    date2=date.split('+')
                    if len(date2)>1:
                        date_d=date2[0]
                        date_final = (datetime.strptime(date_d[:19], "%Y-%m-%d %H:%M:%S"))- relativedelta(hours=int(date2[1][0:2]))#datetime finale au bon format  
                    else:
                        date2=date.split('-')
                        date_d=date2[0]
                        date_final = (datetime.strptime(date_d[:19], "%Y-%m-%d %H:%M:%S"))+ relativedelta(hours=int(date2[1][0:2]))
                    #Si shortcode Shared
                 
                    if j.type_2 =='shared':
                        fw=i['message'].split(' ')
                        id_record=False
                        #on boucle sur les second keyword si ALL n'est pas prÃ©sent
                        for k in j.connect_ids:
                            #si le second keyword est egale a celui de la config
                            if fw[1] == k.name:
                                id_sms=self.create(cr,uid,{'sendadres' : mobile[1] ,
                                                   'mess' :" ".join(i['message'].split()[2:]),
                                                   'datime' : date_final,
                                                   'mess_id' : i['messageId'],
                                                   'config': j.id})
                                id_record=self.pool.get(k.link.model).create(cr,uid,{k.subject.name:j.name,
                                                                   k.message.name:" ".join(i['message'].split()[2:]),
                                                                   k.mobile.name:mobile[1],
                                                                   k.date.name:date_final,
                                                                   })
                                #on verifie qu'on encode le sms_id ou pas
                                if k.sms_id and k.sms_id.name:
                                    self.pool.get(k.link.model).write(cr,uid,id_record,{k.sms_id.name : i['messageId']})
                                #On lie le sms a la config du second keyword  
                                self.write(cr,uid,id_sms,{'link' : k.id})
                                
                            #on verifie si le second keyword est '*' (ALL)
                            elif k.name == '*' and not id_record:
                                id_sms=self.create(cr,uid,{'sendadres' : mobile[1] ,
                                                   'mess' :" ".join(i['message'].split()[1:]),
                                                   'datime' : date_final,
                                                   'mess_id' : i['messageId'],
                                                   'config': j.id})
                                id_record=self.pool.get(k.link.model).create(cr,uid,{k.subject.name:j.name,
                                                                   k.message.name:" ".join(i['message'].split()[1:]),
                                                                   k.mobile.name:mobile[1],
                                                                   k.date.name:date_final,
                                                                   })
                                #on verifie qu'on encode le sms_id ou pas
                                if k.sms_id and k.sms_id.name:
                                    self.pool.get(k.link.model).write(cr,uid,id_record,{k.sms_id.name : i['messageId']})
                                #On lie le sms a la config du second keyword  
                                self.write(cr,uid,id_sms,{'link' : k.id})
                                
                    #si shortcode Dedicated            
                    elif j.type_2=='dedicated':
                        fw=i['message'].split(' ')
                        id_record=False
                        #on boucle sur les seconds keyword si ALL n'est pas prÃ©sent
                        for k in j.connect_ids:
                            #si le second keyword est egale a celui de la config
                            if fw[0] == k.name:
                                id_sms=self.create(cr,uid,{'sendadres' : mobile[1] ,
                                                   'mess' :" ".join(i['message'].split()[1:]),
                                                   'datime' : date_final,
                                                   'mess_id' : i['messageId'],
                                                   'config': j.id})
                                id_record=self.pool.get(k.link.model).create(cr,uid,{k.subject.name:j.name,
                                                                   k.message.name:" ".join(i['message'].split()[1:]),
                                                                   k.mobile.name:mobile[1],
                                                                   k.date.name:date_final,
                                                                   })
                                #on verifie qu'on encode le sms_id ou pas
                                if k.sms_id and k.sms_id.name:
                                    self.pool.get(k.link.model).write(cr,uid,id_record,{k.sms_id.name : i['messageId']})
                                #On lie le sms a la config du second keyword  
                                self.write(cr,uid,id_sms,{'link' : k.id})
                            #on verifie si le second keyword est '*' (ALL)
                            elif k.name == '*' and not id_record:
                                id_sms=self.create(cr,uid,{'sendadres' : mobile[1] ,
                                                   'mess' :" ".join(i['message'].split()[0:]),
                                                   'datime' : date_final,
                                                   'mess_id' : i['messageId'],
                                                   'config': j.id})
                                id_record=self.pool.get(k.link.model).create(cr,uid,{k.subject.name:j.name,
                                                                   k.message.name:" ".join(i['message'].split()[0:]),
                                                                   k.mobile.name:mobile[1],
                                                                   k.date.name:date_final,
                                                                   })
                                #on verifie qu'on encode le sms_id ou pas
                                if k.sms_id and k.sms_id.name:
                                    self.pool.get(k.link.model).write(cr,uid,id_record,{k.sms_id.name : i['messageId']})
                                #On lie le sms a la config du second keyword  
                                self.write(cr,uid,id_sms,{'link' : k.id})
                    
    #        except HTTPError, e:
    #           response.setCode(e.code)
            return response
menu_receipt()