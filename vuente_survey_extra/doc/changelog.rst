v1.2
====
* Fires off campaign during partner survey

v1.1
====
* Partner surveys

v1.0.2
======
* Fix permission issue

v1.0.1
======
* Fix bug where survey ould not save if file select as empty

v1.0
====
* Initial release