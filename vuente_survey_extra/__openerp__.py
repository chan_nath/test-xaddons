{
    'name': "Vuente Survey Extra",
    'version': "1.2",
    'author': "Vuente",
    'category': "Tools",
    'summary': "Conditional questions and file uploads",
    'license':'LGPL-3',
    'data': [
        'views/survey_page_views.xml',
        'views/survey_question_views.xml',
        'views/survey_extra_templates.xml',
        'views/survey_user_input_line_views.xml',
        'views/survey_survey_views.xml',
        'views/survey_user_input_views.xml',
        'views/survey_label_category_views.xml',
        'views/res_partner_views.xml',
    ],
    'demo': [],
    'depends': ['survey','crm','marketing_campaign'],
    'images':[
        'static/description/1.jpg',
    ],
    'installable': True,
}