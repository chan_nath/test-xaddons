{
    'name': "Vuente Project Team",
    'version': "1.0",
    'author': "Vuente",
    'category': "Tools",
    'summary': "Add team kanban view to project form, also add tags to project",
    'license':'LGPL-3',
    'data': [
        'views/project_project_views.xml',
    ],
    'demo': [],
    'depends': ['project'],
    'images':[
        'static/description/1.jpg',
    ],
    'installable': True,
}