# -*- coding: utf-8 -*-
# © 2016 Elico Corp (www.elico-corp.com).
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    "name": "Business Requirement Resources Task Categories",
    "category": "Business Requirement Management",
    "summary": "Adds Task Categories to your Business Requirement Resources",
    "version": "9.0.1.0.0",
    "website": "https://www.elico-corp.com/",
    "author": "Elico Corp, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": [
        "business_requirement_deliverable"
    ],
    "data": [
        "views/business_requirement_resource.xml",
    ],
    "application": False,
    "installable": True,
}
