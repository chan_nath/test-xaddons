{
    'name': 'Theme Odoo Experts eCommerce Plugin',
    'description': 'Odoo Experts eCommerce Plugin',
    'category': 'Hidden',
    'sequence': 211,
    'version': '1.0',
    'author': 'Odoo S.A.',
    'depends': ['theme_loftspace_sale', 'theme_odoo_experts'],
    'demo': [
        'demo/products.xml',
    ],
    'auto_install': True,
}