odoo.define( "ibs_helpdesk.response_quote", function ( require ) {
"use strict";

var KanbanRecord = require( "web_kanban.Record" );

KanbanRecord.include({
    renderElement: function() {
        this._super();

        var $el = this.$el;

        $el.find( "blockquote" ).each( function() {
            $( this ).prev( ".collapse-quote" ).remove();

            $( "<a href=\"./\" class=\"collapse-quote\">···</a>" ).insertBefore( this );
        });

        $el.off( "click", ".collapse-quote" ).on( "click", ".collapse-quote", function( e ) {
            e.preventDefault();
            e.stopPropagation();

            $( this ).next( "blockquote" ).toggle();
        });
    }
});

});