# -*- coding: utf-8 -*-
# Copyright 2013-2016 IBS North Africa
# This is the property of IBS North Africa, any reproduction or modification without
# given consent is strictly prohibited and can be prosecuted to the full extent of the law.
# www.ibs-na.com / contact@ibs-na.com

from openerp import netsvc
from openerp.osv import fields, osv
from openerp.osv.orm import setup_modifiers

from datetime import datetime
from lxml import etree

class ibs_ticket_open_wizard( osv.osv_memory ):
    _name = "ibs.ticket.open.wizard"

    _columns = {
        "category": fields.many2one( "ibs.ticket.category" , "Category", required = True ),
        "agent": fields.many2one( "res.users", "Agent", required = True ),
        "client": fields.many2one( "res.partner" , "Client" )
    }

    _defaults = {
        "category": lambda self, cr, uid, context = {}: ( "ticket_id" in context ) and self.pool.get( "ibs.ticket" ).browse( cr, uid, context[ "ticket_id" ] ).category.id or None
    }

    def fields_view_get( self, cr, uid, view_id = None, view_type = "form", context = None, toolbar = False, submenu = False ):
        if context is None:
            context = {}

        res = super( ibs_ticket_open_wizard, self ).fields_view_get( cr, uid, view_id = view_id, view_type = view_type, context = context, toolbar = toolbar, submenu = False )
        
        if context.get( "ticket_id", False ):
            client = self.pool.get( "ibs.ticket" ).browse( cr, uid, context[ "ticket_id" ] ).client.id or None

            doc = etree.XML( res[ "arch" ] )
            
            nodes = doc.xpath( "//field[@name='client']" )
            
            for node in nodes:
                node.set( "invisible", "0" if not client else "1" )
                setup_modifiers( node, res[ "fields"][ "client" ] )

            res[ "arch" ] = etree.tostring( doc )

        return res

    def onchange_category_agents( self, cr, uid, ids, category, context = None ):
        category_obj = self.pool.get( "ibs.ticket.category" )
        
        agents = category_obj._get_agent_ids( cr, uid, category, context = context )

        return {
            "domain": { "agent": [ ( "id", "in", agents[ "ids" ] ) ] },
            "value": { "agent": agents[ "least_busy" ] }
        }

    def create( self, cr, uid, values, context = None ):
        agent_obj = self.pool.get( "res.users" )
        ticket_obj = self.pool.get( "ibs.ticket" )

        if "ticket_id" in context:
            agent_obj.check_ticket_capacity( cr, uid, values[ "agent" ] )

            to_save = { "category": values[ "category" ], "agent": values[ "agent" ] }

            if "client" in values and values[ "client" ]:
                to_save[ "client" ] = values[ "client" ]

            ticket_obj.write( cr, uid, [ context[ "ticket_id" ] ], to_save, context = context )

            wf_service = netsvc.LocalService( "workflow" )
            wf_service.trg_validate( uid, "ibs.ticket", context[ "ticket_id" ], "open", cr )

        return super( ibs_ticket_open_wizard, self ).create( cr, uid, values, context = context )