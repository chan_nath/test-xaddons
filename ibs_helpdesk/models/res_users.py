# -*- coding: utf-8 -*-
# Copyright 2013-2016 IBS North Africa
# This is the property of IBS North Africa, any reproduction or modification without
# given consent is strictly prohibited and can be prosecuted to the full extent of the law.
# www.ibs-na.com / contact@ibs-na.com

from openerp.osv import fields, osv
from openerp.tools.translate import _

class res_users( osv.osv ):
    _inherit = "res.users"

    def _open_tickets( self, cr, uid, ids, name, arg, context = None ):
        ticket_obj = self.pool.get( "ibs.ticket" )

        counts = {} 

        for line in self.browse( cr, uid, ids, context = context ):
            counts[ line.id ] = len( ticket_obj.search( cr, uid, [ ( "agent", "=", line.id ), ( "state", "not in", [ "draft", "resolved", "cancel" ] ) ] ) )
        
        return counts

    def _remaining_tickets_number( self, cr, uid, ids, name, arg, context = None ):
        ret = {}

        for user in self.browse( cr, uid, ids ):
            open_tickets = user.open_tickets_number
            max_tickets = user.max_tickets

            if max_tickets > open_tickets :
                ret[ user.id ] = max_tickets - open_tickets
            else :
                ret[ user.id ]  = 0

        return ret

    def check_ticket_capacity( self, cr, uid, user_id ):

        remaining_tickets_number = self.browse( cr, uid, user_id ).remaining_tickets_number

        if remaining_tickets_number <= 0:
            raise osv.except_osv(
                _( "Warning" ),
                _( "This agent has reached his maximum ticket capacity." )
            )

        return True

    _columns = {
        "max_tickets" : fields.integer( "Maximum tickets" ),
        "open_tickets_number": fields.function( _open_tickets, string = "Open tickets", type = "integer" ),
        "remaining_tickets_number": fields.function( _remaining_tickets_number, string = "Remaining tickets", store = False, type = "integer", readonly = True ),
        "response_signature": fields.html( "Signature" ),
        "category_ids": fields.many2many( "ibs.ticket.category" , "agent_category_rel" , "helpdesk_agent_id" , "ticket_category_id", "Categories" ),
    }

    _defaults = {
        "max_tickets": 1,
    }

    _sql_constraints = [
        ( "ibs_helpdesk_max_tickets_constraint", "CHECK ( max_tickets > 0 )", "Maximum tickets must be positive." ),
    ]
