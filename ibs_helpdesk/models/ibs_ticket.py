# -*- coding: utf-8 -*-
# Copyright 2013-2016 IBS North Africa
# This is the property of IBS North Africa, any reproduction or modification without
# given consent is strictly prohibited and can be prosecuted to the full extent of the law.
# www.ibs-na.com / contact@ibs-na.com

from openerp import netsvc, SUPERUSER_ID, api
from openerp.osv import fields, osv
from openerp.tools import html_email_clean, DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.mail import email_split
from openerp.tools.translate import _
from openerp import models

import re
import logging
from datetime import datetime, timedelta
from email.utils import getaddresses
import pytz

module_name = "ibs_helpdesk"

_logger = logging.getLogger(__name__)

def ticket_email_split( raw_email ):
    """ Return a list of the email addresses found in ``text`` """
    if not raw_email:
        return []

    addresses = getaddresses( [ raw_email ] )

    email = addresses[ 0 ]

    if email[ 1 ] and "@" in email[ 1 ]:
        return email

    return []

def _interpolate( s, d ):
    if s:
        return s % d
    return ""

def _interpolation_dict( context ):
    now = range_date = effective_date = datetime.now( pytz.timezone( context.get( "tz" ) or "UTC" ) )
    if context.get( "ir_sequence_date" ):
        effective_date = datetime.strptime( context.get( "ir_sequence_date" ), "%Y-%m-%d" )
    if context.get( "ir_sequence_date_range"):
        range_date = datetime.strptime( context.get( "ir_sequence_date_range" ), "%Y-%m-%d" )

    sequences = {
        "year": "%Y", "month": "%m", "day": "%d", "y": "%y", "doy": "%j", "woy": "%W",
        "weekday": "%w", "h24": "%H", "h12": "%I", "min": "%M", "sec": "%S"
    }
    res = {}
    for key, sequence in sequences.iteritems():
        res[key] = effective_date.strftime( sequence )
        res[ "range_" + key] = range_date.strftime( sequence )
        res[ "current_" + key ] = now.strftime( sequence )

    return res

IBS_TICKET_STATES = [
    ( "draft", "Draft" ),
    ( "opened", "Open" ),
    ( "awaiting_response", "Awaiting response" ),
    ( "answered", "Responded" ),
    ( "resolved", "Resolved" ),
    ( "cancel", "Canceled" ),
]

class ibs_ticket( osv.osv ):
    _name= "ibs.ticket"
    _order = "id DESC"
    _description = "Tickets"
    _inherit = [ "mail.thread", "ir.needaction_mixin" ]
    _order = "date desc"

    def _get_deadline( self, cr, uid, ids, name, arg, context = None ):
        deadlines = {} 
        category_obj = self.pool.get( "ibs.ticket.category" )

        for line in self.browse( cr, uid, ids, context = context ):
            # Only calculate deadline if category is present.
            if line.category:
                category = category_obj.browse( cr, uid, line.category.id, context = context )
                value_day = category.deadline.value_day
                value_hours = category.deadline.value_hours

                deadline = datetime.strptime( line.date, DEFAULT_SERVER_DATETIME_FORMAT ) + timedelta( days = value_day ) + timedelta( hours = value_hours ) 
                deadlines[ line.id ] = deadline.strftime( DEFAULT_SERVER_DATETIME_FORMAT )
            else:
                deadlines[ line.id ] = line.date

        return deadlines
    # HACK: due to recursive problem whnen using write method weuse instead the sql query
    def _set_deadline( self, cr, uid, id, field_name, field_value, args = None,
                       context = None ):
        if field_value:
            cr.execute( "UPDATE ibs_ticket SET deadline = %s WHERE id = %s",
                        ( field_value, id, ) )

        return True

    def _age_ticket( self, cr, uid, ids, name, arg, context = None ):
        ages = {}
        
        now = datetime.now()

        for line in self.browse( cr, uid, ids, context = context ):
            age = ""
            hours = 0
            minutes = 0

            if line.date:
                diff = now - datetime.strptime( line.date, DEFAULT_SERVER_DATETIME_FORMAT )

                if diff.seconds >= 3600:
                    hours = ( diff.seconds / 60 ) / 60
                else:
                    minutes = diff.seconds / 60

                if diff.days <= 1:
                    age = _( "%s h %s m" ) % ( hours, minutes )
                else:
                    age = _( "%s d %s h" ) % ( diff.days, hours )

            ages[ line.id ] = age
                
        return ages

    def run_scheduler( self, cr, uid, context = None ):
        _logger.info( _( "Late tickets scheduler is running..." ) )
        
        tickets = self.search( cr, uid, [ ( "state", "in", [ "opened", "awaiting_response" ] ) ] )
        
        today = datetime.now().strftime( DEFAULT_SERVER_DATETIME_FORMAT )

        for line in self.browse( cr, uid, tickets, context = context ):
            if ( line.deadline < today ):
                late_ticket_template = line.agent.company_id.late_ticket_template

                if late_ticket_template:
                    template_obj = self.pool.get( "mail.template" )
                    mail_obj = self.pool.get( "mail.mail" )

                    mail_id = template_obj.send_mail( cr, uid, late_ticket_template.id, line.id, True, context = context )
                else:
                    _logger.warning( _( "Late tickets cron: Please configure a \
                        template in your company helpdesk settings." ) )
        
        return True

    def _is_late( self, cr, uid, ids, name, arg, context = None ):
        results = {}

        for ticket in self.browse( cr, uid, ids, context = context ):
            if not ticket.deadline:
                results[ ticket.id ] = False
                continue

            deadline = datetime.strptime( ticket.deadline, DEFAULT_SERVER_DATETIME_FORMAT )
            results[ ticket.id ] = ( deadline < datetime.now() ) and ( ticket.state in [ "opened", "awaiting_response" ] )

        return results

    _columns = {
        "name": fields.char( "Ref." , size = 20 , required = True , readonly = True , select = True ),
        "state": fields.selection( IBS_TICKET_STATES, "State", readonly = True , track_visibility = "onchange" ),
        "client": fields.many2one( "res.partner" , "Client" , readonly = False,
            states = {
                "resolved": [ ( "readonly", True ) ],
                "cancel": [ ( "readonly", True ) ]
            },
            ondelete = "restrict"
        ),
        "email": fields.char( "Email", size = 255 ),
        "subject": fields.char( "Subject" , size = 255, readonly = False,
            states = {
                "resolved": [ ( "readonly", True ) ],
                "cancel": [ ( "readonly", True ) ]
            }
        ),
        "category": fields.many2one( "ibs.ticket.category" , "Category", readonly = False,
            states = {
                "resolved": [ ( "readonly", True ) ],
                "cancel": [ ( "readonly", True ) ]
            },
            ondelete = "restrict"
        ),
        "source": fields.many2one( "ibs.ticket.source", "Source", readonly = False,
            states = {
                "resolved": [ ( "readonly", True ) ]
            }
        ),
        "agent": fields.many2one( "res.users", "Agent" , readonly = True ),
        "priority": fields.many2one( "ibs.ticket.priority", "Priority", readonly = False,
            states = {
                "resolved": [ ( "readonly", True ) ]
            }
        ),
        "date": fields.datetime( "Date" , readonly = True,
            states = {
                "draft": [ ( "readonly", False ) ]
            }
        ),
        "deadline": fields.function( _get_deadline , fnct_inv = _set_deadline,
                                    store = {
                                        "ibs.ticket": ( 
                                            lambda self, cr, uid, ids, c = {}: ids,
                                            [ "category" ], 1 
                                        ),
                                    }, 
                                    states = {
                                        "cancel": [ ( "readonly", True ) ],
                                        "resolved": [ ( "readonly", True ) ]
                                    }, type = "datetime" , string = "Deadline" ),
        "age": fields.function( _age_ticket, type = "char" , string = "Age" ),
        "resolution_date": fields.datetime( "Resolution" , readonly = True,
            states = {
                "resolved": [ ( "readonly", False ) ]
            }
        ),
        "responses": fields.one2many( "ibs.ticket.response", "ticket_id", "Responses" ),
        "responses_count": fields.integer( string = "Responses count" ),
        "company_id": fields.many2one( "res.company", "Company", required = True ),
        "is_late": fields.function( _is_late, type = "boolean", string = "Is late" ),
        "notes": fields.html( "Notes" )
    }
    _defaults = {
        "name": "/",
        "date": lambda *a: datetime.now().strftime( DEFAULT_SERVER_DATETIME_FORMAT ),
        "state": "draft",
        "company_id": lambda self, cr, uid, context = {}: self.pool.get( "res.users" ).browse( cr, uid, uid ).company_id.id,
        "responses_count": 0
    }

    _sql_constraints = [
        ( "name_uniq" , "UNIQUE( name, company_id )", "This reference already exists." ),
    ]

    def create( self, cr, uid, values, context = None ):
        values[ "name" ] = self.pool.get( "ir.sequence" ).get( cr, uid, "ibs.ticket.reference" )

        return super( ibs_ticket, self ).create( cr, uid, values, context = context )

    def open( self, cr, uid, ids, context = None ):
        mod_obj = self.pool.get( "ir.model.data" )
        act_obj = self.pool.get( "ir.actions.act_window" )

        ticket = self.browse( cr, uid, ids[ 0 ], context = context )

        if ticket.state != "draft":
            raise osv.except_osv(
                _( "Warning" ),
                _( "This ticket has already been opened." )
            )

        result = mod_obj.get_object_reference( cr, uid, module_name, "action_ibs_ticket_open_wizard" )
        id = result and result[ 1 ] or False
        
        result = act_obj.read( cr, uid, [ id ], context = context )[ 0 ]
        result[ "target" ] = "new"
        result[ "nodestroy"] = True

        context[ "ticket_id" ] = ticket.id

        result[ "context" ] = context
        
        return result

    def response( self, cr, uid, ids, context = None ):
        mod_obj = self.pool.get( "ir.model.data" )
        act_obj = self.pool.get( "ir.actions.act_window" )

        ticket = self.browse( cr, uid, ids[ 0 ], context = context )

        if ticket.state in ( "draft", "resolved", "cancel" ):
            raise osv.except_osv(
                _( "Warning" ),
                _( u"This ticket is not open." )
            )

        result = mod_obj.get_object_reference( cr, uid, module_name, "action_ibs_ticket_response" )
        id = result and result[ 1 ] or False
        
        result = act_obj.read( cr, uid, [ id ], context = context )[ 0 ]
        result[ "target" ] = "new"
        result[ "nodestroy"] = True

        context[ "ticket_id" ] = ticket.id

        # Fill body with the signature and the previous response.
        first_message = ticket.responses and ticket.responses[ 0 ] and ticket.responses[ 0 ].body_html or ""

        body = ""
        # In same cases we don't want to add the signature.
        if "no_signature" not in context:
            body = "<br/>%s" % ( ticket.agent.response_signature or "", )

        if first_message:
            body = body + "<br/><blockquote style=\"margin-top: 10px; \
                margin-bottom: 10px;margin-left: 0px;padding-left: 15px; \
                border-left: 3px solid #ccc;\">%s</blockquote>" % (  first_message )

        context[ "default_body_html" ] = body

        result[ "context" ] = context
        
        return result

    def unlink( self, cr, uid, ids, context = None, force = False ):
        to_delete = []

        for line in self.browse( cr, uid, ids, context = context ):
            if not force and line.state not in ( "draft" ):
                raise osv.except_osv(
                    _( "Warning" ),
                    _( "You cannot delete this ticket!" )
                )

            to_delete.append( line.id )

        return super( ibs_ticket, self ).unlink( cr, uid, to_delete, context=context )

    def wkf_resolve_ticket( self, cr, uid, ids, context = None ):
        self.write( cr, uid, ids, {
            "state": "resolved",
            "resolution_date": datetime.now().strftime( DEFAULT_SERVER_DATETIME_FORMAT )
        }, context = None )

        return True

    def _process_raw_mail( self, cr, uid, msg, context = None ):
        mail = {}
        ticket_data = {}
        response_data = {}

        mail[ "subject" ] = msg.get( "subject" )
        ticket_data[ "subject" ] = mail[ "subject" ]

        # In order to avoid long non wrapping lines.
        body = msg.get( "body" )
        response_data[ "body_html" ] = body and html_email_clean( body ).replace( "&nbsp;", " " ).replace( "&#160;", " " ) or ""

        # Hack: Replace comma since some people have them in their full names, which breaks our parser.
        email = ticket_email_split( msg.get( "from" ).replace( ",", "" ) )
        mail[ "from" ] = email and email[ 1 ] or msg.get( "from" )

        ticket_data[ "email" ] = mail[ "from" ]

        response_data[ "agent" ] = None

        mail[ "response_data" ] = response_data
        mail[ "ticket_data" ] = ticket_data

        mail[ "cc" ] = msg.get( "cc" )

        return mail

    def _get_ticket_id( self, cr, uid, subject, context = None ):
        mod_obj = mod_obj = self.pool.get( "ir.model.data" )
        seq_obj = self.pool.get( "ir.sequence" )

        ticket_id = None

        sequence_results = mod_obj.get_object_reference( cr, uid, module_name, 
                                                         "sequence_ibs_ticket" )
        sequence_id = sequence_results and sequence_results[ 1 ] or False

        if not sequence_id:
            raise osv.except_osv( 
                _( "Warning" ),
                _( "Ticket sequence is not defined." )
            )

        sequence = seq_obj.browse( cr, uid, sequence_id, context = context )

        subject_ticket_number = False

        if subject:
            d = _interpolation_dict( context )
            
            try:
                interpolated_prefix = _interpolate( sequence.prefix, d )
                interpolated_suffix = _interpolate( sequence.suffix, d )

                pattern = "%s[0-9]{%s}%s" % (
                    re.escape( interpolated_prefix ),
                    str( sequence.padding ),
                    re.escape( interpolated_suffix )
                )

                subject_ticket_number = re.search( pattern, subject )
            except ValueError:
                raise osv.except_osv( _( "Warning" ), _( "Invalid ticket sequence." ) )

        if subject_ticket_number:
            ticket_results = self.search( cr, uid, [ ( "name", "=", subject_ticket_number.group() ) ], limit = 1 )
            ticket_id = ticket_results and ticket_results[ 0 ] or False

        return ticket_id

    def _get_client( self, cr, uid, email_from, context = None ):
        partner_obj = self.pool.get( "res.partner" )

        client = partner_obj.search( cr, uid, [ ( "email", "=", email_from ) ], limit = 1 )
        client = client and client[ 0 ] or None

        return client

    def _process_new_ticket( self, cr, uid, ticket_data, context = None ):
        ticket_id = None

        try:
            ticket_id = self.create( cr, uid, ticket_data, context = context )
        except Exception as err:
            return _logger.info( err )

        return ticket_id

    def _process_existing_ticket( self, cr, uid, ticket_id, context = None ):
        wf_service = netsvc.LocalService( "workflow" )
        wf_service.trg_validate( uid, "ibs.ticket", ticket_id, "response_received", cr )

        return False

    def _create_response( self, cr, uid, mail, context = None ):
        response_obj = self.pool.get( "ibs.ticket.response" )

        response_id = response_obj.create( cr, uid, mail[ "response_data" ], send_email = False, send_auto_response = mail[ "send_auto_response" ], context = context )

        return response_id

    # Process incoming emails.
    def message_new( self, cr, uid, msg, custom_values = None, context = None ):
        """
            - Process incoming email
            - Find ticket based on the subject
                - Create a new ticket if not found
            - Save response
        """

        mail = self._process_raw_mail( cr, uid, msg, context = context )
        ticket_data = mail[ "ticket_data" ]

        ticket_id = None
        # We have already done the processing somewhere else before. for instance when processing email black list.
        if "ticket_id" not in context:
            ticket_id = self._get_ticket_id( cr, uid, ticket_data[ "subject" ], context = context )
        else:
            ticket_id = context[ "ticket_id" ]

        # Depending on configuration, the automatic response 
        # might be sent once only when a new ticket is received.
        send_auto_response = True

        ticket_data[ "client" ] = self._get_client( cr, uid, mail[ "from" ], context = context )

        if not ticket_id:
            ticket_id = self._process_new_ticket( cr, uid, ticket_data, context = context )
        else:
            # This process decides if we will send an automatic response as well.
            send_auto_response = self._process_existing_ticket( cr, uid, ticket_id, context = context )

        mail[ "send_auto_response" ] = send_auto_response
        mail[ "ticket_data" ] = ticket_data
        mail[ "response_data" ][ "ticket_id" ] = ticket_id

        self._create_response( cr, uid, mail, context = context )

    # This method saves incoming messages in mail.message ( look in mail_thread.py ), we stop saving them in order to avoid a bug where if the customer responds to his own message,
    # Odoo considers it a response to an existing message which prevents message_new() from being executed.
    @api.cr_uid_ids_context
    def message_post( self, cr, uid, thread_id, message_type = "notification", subtype = None, context = None, **kwargs ):

        if message_type in [ "notification", "comment" ]: # Incoming emails are not saved, but only processedb by message_new()
            return super( ibs_ticket, self ).message_post( cr, uid, thread_id, message_type = message_type, subtype = subtype, context = context, **kwargs )

        return bogusMessageClass()

# Hack to prevent error in line 1140 new_msg.write({'partner_ids': partner_ids})
# in file mail/models/mail_thread.py
class bogusMessageClass():
    def write( *args, **kwargs ):
        pass

class ibs_ticket_source( osv.osv ):
    _name = "ibs.ticket.source"

    _columns = {
        "name": fields.char( "Name" )
    }
