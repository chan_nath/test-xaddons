# -*- coding: utf-8 -*-
# Copyright 2013-2016 IBS North Africa
# This is the property of IBS North Africa, any reproduction or modification without
# given consent is strictly prohibited and can be prosecuted to the full extent of the law.
# www.ibs-na.com / contact@ibs-na.com

from openerp import netsvc, SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.translate import _

import base64
from datetime import datetime

class ibs_ticket_response( osv.osv ):
    _name = "ibs.ticket.response"

    _order = "id desc"
    
    _columns = {
        "body_html": fields.html( "Message" , required = True ),
        "date": fields.datetime( "Date" , readonly = True ),
        "ticket_id": fields.many2one( "ibs.ticket", "Ticket" , invisible = True ),
        "agent": fields.many2one( "res.users" , "Agent" , readonly = True ),
        "company_id": fields.related( "ticket_id", "company_id", type = "many2one", relation = "res.company", string = "Company" ),
        "state": fields.related( "ticket_id", "state", type = "char", string = "State" ),
        "lang": fields.char( "Language" )
    }

    def default_get( self, cr, uid, fields, context = None ):
        user_obj = self.pool.get( "res.users" )

        ticket_id = ( "ticket_id" in context ) and context[ "ticket_id" ] or None

        user = user_obj.browse( cr, uid, uid, context = context )

        defaults = {
            "date": datetime.now().strftime( DEFAULT_SERVER_DATETIME_FORMAT ),
            "agent": uid,
            "ticket_id": ticket_id,
            "lang": user.partner_id.lang
        }

        if "default_body_html" in context:
            defaults[ "body_html" ] = context[ "default_body_html" ]

        return defaults

    def send( self, cr, uid, email_from, email_to, subject, body, mail_server_id, reply_to = None, attachments = None, email_bcc = None, email_cc = None, context = None ):
        ir_mail_server = self.pool.get( "ir.mail_server" )
        
        msg = ir_mail_server.build_email(
            email_from = email_from,
            email_to = email_to,
            reply_to = reply_to,
            subject = subject,
            body = body,
            email_bcc = email_bcc,
            email_cc = email_cc,
            attachments = attachments,
            subtype = "html",
            subtype_alternative = "plain",
            object_id = None,
            headers = { "h:Reply-To": reply_to } # This is for custom mail servers such as mailgun.
        )

        res = ir_mail_server.send_email( cr, uid, msg, mail_server_id = mail_server_id, context = context )
        
        if not res:
            raise osv.except_osv(
                _( "Warning" ),
                _( "Please setup your helpdesk." )
            )

    def _check_email_validity( self, cr, uid, values, context = None ):
        fetchmail_server_obj = self.pool.get( "fetchmail.server" )

        found = False

        if "email" in values:
            found = fetchmail_server_obj.search(
                cr, SUPERUSER_ID,
                [ ( "user", "=", values[ "email" ] ) ],
                count = 1, context = context
            )

        if found:
            raise osv.except_osv(
                _( "Warning" ),
                _( "This email address is associated with an incoming email server." )
            )

    def _update_ticket( self, cr, uid, response, send_email, update_count = True, context = None ):
        ticket_obj = self.pool.get( "ibs.ticket" )

        ticket = response.ticket_id

        # Update the ticket count.
        to_write = {}

        if update_count:
            to_write[ "responses_count" ] = ticket.responses_count + 1
        
        # When the user is the first to respond and the agent isn't set yet,
        # make him the ticket's agent.
        if send_email and not ticket.agent:
            to_write[ "agent" ] = uid

        if to_write:
            ticket_obj.write( cr, uid, [ ticket.id ], to_write, context = context )

        return ticket

    # Check if we have configured an outgoing email server.
    def _check_outgoing_mail_server( self, cr, uid, data, mail_server_id = False, context = None ):
        user_obj = self.pool.get( "res.users" )
        mail_server_obj = self.pool.get( "ir.mail_server" )

        user = user_obj.browse( cr, uid, uid, context = context )

        # We didn't get another mail server, fallback to default.
        if not mail_server_id:
            mail_server_id = user.company_id.default_mail_server and user.company_id.default_mail_server.id or None

        if not mail_server_id:
            raise osv.except_osv(
                _( "Warning" ),
                _( "Please setup an outgoing email server." )
            )

        return mail_server_obj.browse( cr, SUPERUSER_ID, mail_server_id, context = context )

    # Prepare the FROM field of the email we are sending.
    def _prepare_from_email( self, cr, uid, data, context = None ):
        mail_server = data[ "mail_server" ]

        # We either use Reply To incase of mail services like mailgun
        # or the smpt_user in normal cases.

        return "%s <%s>" % ( mail_server.name, mail_server.x_reply_to or mail_server.smtp_user )

    # Prepare the TO field of the email we are sending.
    def _prepare_to_email( self, cr, uid, data, context = None ):
        response = data[ "response" ]

        return response.ticket_id.email

    # Prepare the SUBJECT field of the email we are sending.
    def _prepare_subject( self, cr, uid, data, context = None ):
        ticket = data[ "ticket" ]

        subject = _( "Ticket: " ) + ticket.name
        
        if ticket.subject:
            subject = _( "Ticket %s: %s " ) % ( ticket.name, ticket.subject )

        return subject

    # Since we have responded to the ticket, its state must reflect it.
    def _update_ticket_workflow( self, cr, uid, data, context = None ):
        ticket_id = data[ "ticket" ].id

        wf_service = netsvc.LocalService( "workflow" )
        
        wf_service.trg_validate( uid, "ibs.ticket", ticket_id, "answer_posted", cr )

    def _prepare_email( self, cr, uid, data, context = None ):
        return data

    def _send_email( self, cr, uid, data, context = None ):
        self.send( cr, uid,
            email_from = data[ "email_from" ], 
            email_to = [ data[ "email_to" ] ],
            reply_to = data[ "email_from" ],
            subject = data[ "subject" ], 
            body = data[ "response" ].body_html, 
            mail_server_id = data[ "mail_server" ].id,
            context = context
        )

    def create( self, cr, uid, values, send_email = True, send_auto_response = False, context = None ):
        data = {}

        # Save the response and hold its ID to be used within the email model.
        response_id = super( ibs_ticket_response, self ).create( cr, uid, values, context = context )
        data[ "response" ] = self.browse( cr, uid, response_id, context = context )

        self._check_email_validity( cr, uid, values, context = context )

        data[ "ticket" ] = self._update_ticket( cr, uid, data[ "response" ], send_email, context = context )

        if send_email:
            data[ "mail_server" ] = self._check_outgoing_mail_server( cr, uid, data, context = context )
            
            data[ "email_from" ] = self._prepare_from_email( cr, uid, data, context = context )

            data[ "email_to" ] = self._prepare_to_email( cr, uid, data, context = context )

            data[ "subject" ] = self._prepare_subject( cr, uid, data, context = context )

            data = self._prepare_email( cr, uid, data, context = context )

            self._send_email( cr, uid, data, context = context )

            self._update_ticket_workflow( cr, uid, data, context = context )

        if send_auto_response:
            self.send_customer_automatic_response( cr, uid, response_id, context = context )

        return response_id

    def save( self, cr, uid, ids, context = None ):
        return True

    def send_customer_automatic_response( self, cr, uid, response_id, 
                                          automatic_response = False, context = None ):
        user_obj = self.pool.get( "res.users" )
        category_obj = self.pool.get( "ibs.ticket.category" )
        template_obj = self.pool.get( "mail.template" )
        mail_obj = self.pool.get( "mail.mail" )

        user = user_obj.browse( cr, uid, uid )

        if not automatic_response:
            automatic_response = user.company_id.automatic_response and \
                                 user.company_id.automatic_response.id or None

        if automatic_response:
            mail_id = template_obj.send_mail( cr, uid, automatic_response, response_id,
                                              True, context = context )

            mail_state = mail_obj.read( cr, uid, mail_id, [ "state" ], context = context )

            if mail_state and mail_state[ "state" ] == "exception":
                raise osv.except_osv(
                    _( "Warning" ),
                    _( "Please setup your helpdesk." )
                )
