# -*- coding: utf-8 -*-
# Copyright 2013-2016 IBS North Africa
# This is the property of IBS North Africa, any reproduction or modification without
# given consent is strictly prohibited and can be prosecuted to the full extent of the law.
# www.ibs-na.com / contact@ibs-na.com

import ibs_ticket
import ibs_ticket_category
import ibs_ticket_deadline
import ibs_ticket_priority
import ibs_ticket_response

import res_company
import res_users