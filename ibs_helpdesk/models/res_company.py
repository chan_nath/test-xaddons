# -*- coding: utf-8 -*-
# Copyright 2013-2016 IBS North Africa
# This is the property of IBS North Africa, any reproduction or modification without
# given consent is strictly prohibited and can be prosecuted to the full extent of the law.
# www.ibs-na.com / contact@ibs-na.com

from openerp.osv import fields, osv

class res_company( osv.osv ):
    _inherit = "res.company"

    _columns = {
        "default_mail_server" : fields.many2one( "ir.mail_server", "Outgoing email server" ),
        "automatic_response": fields.many2one( "mail.template" , "Automatic response" ),
    }


class ir_mail_server( osv.osv ):
    _inherit = "ir.mail_server"

    _columns = {
        "x_reply_to" : fields.char( "Reply to" ),
    }
