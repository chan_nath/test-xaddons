# -*- coding: utf-8 -*-
# Copyright 2013-2016 IBS North Africa
# This is the property of IBS North Africa, any reproduction or modification without
# given consent is strictly prohibited and can be prosecuted to the full extent of the law.
# www.ibs-na.com / contact@ibs-na.com

from openerp.osv import fields, osv
from datetime import datetime
                    
class ibs_ticket_deadline( osv.osv ):
    _name= "ibs.ticket.deadline"
    
    _columns = {
        "name": fields.char( "Name" , size = 10 , required = True ),
        "value_day": fields.integer( "Days" ),
        "value_hours": fields.integer( "Hours" ),
    }
