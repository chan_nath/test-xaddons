# -*- coding: utf-8 -*-
# Copyright 2013-2016 IBS North Africa
# This is the property of IBS North Africa, any reproduction or modification without
# given consent is strictly prohibited and can be prosecuted to the full extent of the law.
# www.ibs-na.com / contact@ibs-na.com

from openerp.osv import fields, osv

class ibs_ticket_priority( osv.osv ):
    _name= "ibs.ticket.priority"

    _columns = {
        "name": fields.char( "Name", size = 50, required = True )
    }
