# -*- coding: utf-8 -*-
# Copyright 2013-2016 IBS North Africa
# This is the property of IBS North Africa, any reproduction or modification without
# given consent is strictly prohibited and can be prosecuted to the full extent of the law.
# www.ibs-na.com / contact@ibs-na.com

from openerp.osv import osv, fields

class ibs_ticket_category( osv.osv ):
    _name = "ibs.ticket.category"

    _columns = {
        "name": fields.char( "Name", size = 40 , required = True ),
        "deadline": fields.many2one( "ibs.ticket.deadline" , "Deadline", required = True ),
        "agent_ids": fields.many2many( "res.users" , "agent_category_rel" , "ticket_category_id" , "helpdesk_agent_id" , "Agents" ),
    }

    def write( self, cr, uid, ids, values, context = None ):
        # Clear record rules cache so that users can see appropriate tickets if we change agents list.
        self.pool.get( "ir.rule" ).clear_caches()

        return super( ibs_ticket_category, self ).write( cr, uid, ids, values, context = context )

    def _get_agent_ids( self, cr, uid, category_id, context = None ):
        agent_obj = self.pool.get( "res.users" )

        results = {
            "ids": [],
            "least_busy": None 
        }

        if category_id:
            agent_ids = [ agent.id for agent in self.browse( cr, uid, category_id, context = context ).agent_ids ]

            least_busy = False

            # Find the agent with the lowest number of open tickets.
            if len( agent_ids ):
                least_busy = agent_obj.browse( cr, uid, agent_ids[ 0 ], context = context )

                for agent in agent_obj.browse( cr, uid, agent_ids, context = context ):
                    if agent.open_tickets_number < least_busy.open_tickets_number:
                        least_busy = agent

                results[ "least_busy" ] = least_busy.id

            results[ "ids" ] = agent_ids

        return results