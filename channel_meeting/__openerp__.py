{
    'name': 'Channel Meeting',
    'version': '9.0',
    'author': 'OmInfoWay',
    'summary': 'Set Meeting for Channel',
    'category': 'Calendar',
    'depends': ['calendar'],
    'data': [
        'views/calendar_event_view.xml',
    ],
    'description': """
        This Module is For adding attendee in meeting from selected channel...
    """,
    'images': ['static/img/main.jpg'],
    'auto_install': True,
    'installable': True,
}
