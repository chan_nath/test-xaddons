# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import models, fields, api

class CalendarEvent(models.Model):
    _inherit = 'calendar.event'

    channel_id = fields.Many2one('mail.channel', string='Channel', domain=[('channel_type', '=', 'channel')])

    @api.onchange('channel_id')
    def onchange_channel_id(self):
        channel_list = []
        partner_ids =[]
        channels = self.env['mail.channel'].browse(self.channel_id.ids)
        if channels.channel_partner_ids:
            partner_ids = [partner.id for partner in channels.channel_partner_ids]
            channel_list.append((6 , 0, partner_ids))
            self.partner_ids = channel_list
